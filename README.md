# Prediction of strokes by Machine Learning

By means of several machine learning methods, we model the possibility of predicting strokes in a dataset containing a very low presence of stroke cases.
